import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./Guard/auth.guard";
import { LogedGuard } from "./Guard/loged.guard";

const routes: Routes = [
	{ path: "", redirectTo: "dashboard", pathMatch: "full" },
	// Auth
	{
		path: "auth",
		children: [
			{
				path: "login",
				loadChildren:
					"./pages/auths/login/login.module#LoginPageModule",
			},
			{
				path: "forgot-password",
				loadChildren:
					"./pages/auths/forgot-password/forgot-password.module#ForgotPasswordPageModule",
			},
		],
	},
	//Dashboard
	{
		path: "dashboard",
		loadChildren: "./pages/home/home.module#HomePageModule",
		canActivate: [AuthGuard],
	},
	//Notifications
	{
		path: "notifications",
		loadChildren:
			"./pages/notifications/notifications.module#NotificationsPageModule",
		canActivate: [AuthGuard],
	},
	//Perfil
	{
		path: "perfil",
		canActivate: [AuthGuard],
		children: [
			{
				path: ":id",
				children: [
					{
						path: "",
						loadChildren:
							"./pages/profiles/show/perfil.module#PerfilPageModule",
					},
					{
						path: "estudios",
						loadChildren:
							"./pages/profiles/asset/estudios/estudios.module#EstudiosPageModule",
					},
					{
						path: "trabajos",
						loadChildren:
							"./pages/profiles/asset/trabajos/trabajos.module#TrabajosPageModule",
						canActivate: [AuthGuard],
					},
				],
			},
			{
				path: "editar/:id",
				loadChildren:
					"./pages/profiles/edit/edit.module#EditarMiPerfilPageModule",
			},
		],
	},
	//Settings
	{
		path: "settings",
		canActivate: [AuthGuard],
		children: [
			{
				path: "users",
				children: [
					{
						path: "",
						loadChildren:
							"./pages/settings/user/show/show.module#ShowPageModule",
					},
					{
						path: "create",
						loadChildren:
							"./pages/settings/user/create/create.module#CreatePageModule",
					},
				],
			},
			{
				path: "directions",
				children: [
					{
						path: "",
						loadChildren:
							"./pages/settings/directions/directions.module#DirectionsPageModule",
					},
				],
			},
			{
				path: "rates",
				children: [
					{
						path: "",
						loadChildren:
							"./pages/settings/rates/rates.module#RatesPageModule",
					},
				],
			},
			{
				path: "memberships",
				children: [
					{
						path: "",
						loadChildren:
							"./pages/settings/memberships/show/show.module#ShowPageModule",
					},
					{
						path: "create",
						loadChildren:
							"./pages/settings/memberships/create/create.module#CreatePageModule",
					},
					{
						path: "edit/:id",
						loadChildren:
							"./pages/settings/memberships/edit/edit.module#EditPageModule",
					},
				],
			},
		],
	},
	//Doctors
	{
		path: "doctors",
		loadChildren: "./pages/doctors/doctors.module#DoctorsPageModule",
		canActivate: [AuthGuard],
	},
	//NOtify Chats
	{
		path: "notify-chats",
		loadChildren:
			"./pages/notify-chats/notify-chats.module#NotifyChatsPageModule",
		canActivate: [AuthGuard],
	},
	//Maps
	{
		path: "maps",
		loadChildren: "./pages/maps/maps.module#MapsPageModule",
		canActivate: [AuthGuard],
	},
	//Mobile-menu
	{
		path: "mobile-menu",
		loadChildren:
			"./pages/mobile-menu/mobile-menu.module#MobileMenuPageModule",
	},
	//Chats
	{
		path: "chat",
		loadChildren: "./pages/chat/chat.module#ChatPageModule",
		canActivate: [AuthGuard],
	},
	//Membresias
	{
		path: "memberships",
		canActivate: [AuthGuard],
		children: [
			{
				path: "",
				redirectTo: "contracts",
				pathMatch: "full",
			},
			{
				path: "contracts",
				children: [
					{
						path: "",
						loadChildren:
							"./pages/membresias/users/users.module#UsersPageModule",
					},
					{
						path: ":user",
						children: [
							{
								path: "",
								loadChildren:
									"./pages/membresias/contracts/contracts.module#ContractsPageModule",
							},
							{
								path: ":country",
								children: [
									{
										path: "",
										loadChildren:
											"./pages/membresias/membresias/membresias.module#MembresiasPageModule",
									},
								],
							},
						],
					},
				],
			},
		],
	},
	// Contabilidad
	{
		path: "accounting",
		canActivate: [AuthGuard],
		children: [
			{
				path: "orders",
				children: [
					{
						path: ":id",
						loadChildren:
							"./pages/accounting/orders/show/show.module#ShowPageModule",
					},
				],
			},
		],
	},
	{
		path: "marketing",
		canActivate: [AuthGuard],
		children: [
			{
				path: "",
				redirectTo: "leads",
				pathMatch: "full",
			},
			{
				path: "leads",
				children: [
					{
						path: "",
						loadChildren:
							"./pages/marketing/leads/show/show.module#ShowPageModule",
					},
					{
						path: "load",
						loadChildren:
							"./pages/marketing/leads/load/load.module#LoadPageModule",
					},
				],
			},
		],
	},
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
	],
	exports: [RouterModule],
})
export class AppRoutingModule {}
