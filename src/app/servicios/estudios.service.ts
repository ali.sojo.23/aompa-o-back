import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Observable";

@Injectable({
	providedIn: "root",
})
export class EstudiosService {
	constructor(private http: HttpClient) {}

	url = environment.urlHost;

	urlEducation: string = this.url + "education/";
	header: any;
	setHeaders() {
		let token = sessionStorage.getItem("tk_init");
		this.header = new HttpHeaders()
			.set("Accept", "application/json")
			.set("access-token", token);
	}
	createInfo(data): Observable<any> {
		this.setHeaders();
		return this.http.post(this.urlEducation, data, {
			headers: this.header,
		});
	}
	getEstudios(identificador): Observable<any> {
		this.setHeaders();
		return this.http.get(this.urlEducation + identificador, {
			headers: this.header,
		});
	}
}
