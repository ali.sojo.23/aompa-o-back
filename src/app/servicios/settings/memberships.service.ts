import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { Observable } from "rxjs";

@Injectable({
	providedIn: "root",
})
export class MembershipsService {
	constructor(private http: HttpClient) {}

	url = environment.urlHost;
	urlMemberships = this.url + "settings/memberships/";
	headers: any;
	setHeaders() {
		let token = sessionStorage.getItem("tk_init");
		this.headers = new HttpHeaders()
			.set("Accept", "application/json")
			.set("access-token", token);
	}
	create(data): Observable<any> {
		this.setHeaders();
		return this.http.post(this.urlMemberships, data, {
			headers: this.headers,
		});
	}
	obtain(): Observable<any> {
		this.setHeaders();
		return this.http.get(this.urlMemberships, { headers: this.headers });
	}
	obtainQuery(id): Observable<any> {
		this.setHeaders();
		return this.http.get(this.urlMemberships + "?country=" + id, {
			headers: this.headers,
		});
	}
	getById(id): Observable<any> {
		this.setHeaders();
		return this.http.get(this.urlMemberships + id, {
			headers: this.headers,
		});
	}
	DeleteOne(id): Observable<any> {
		this.setHeaders();
		return this.http.delete(this.urlMemberships + id, {
			headers: this.headers,
		});
	}
	UpdateOne(id, data): Observable<any> {
		this.setHeaders();
		return this.http.post(this.urlMemberships + id, data, {
			headers: this.headers,
		});
	}
}
