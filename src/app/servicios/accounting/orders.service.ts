import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";

@Injectable({
	providedIn: "root",
})
export class OrdersService {
	constructor(private http: HttpClient) {}
	urlOrder = environment.urlHost + "accounting/orders/";
	headers: any;
	setHeaders() {
		let token = sessionStorage.getItem("tk_init");
		this.headers = new HttpHeaders()
			.set("Accept", "application/json")
			.set("access-token", token);
	}
	create(data): Observable<any> {
		this.setHeaders();
		return this.http.post(this.urlOrder, data, {
			headers: this.headers,
		});
	}
	cancelar(id) {
		this.setHeaders();
		return this.http.delete(this.urlOrder + id, {
			headers: this.headers,
		});
	}
	getOne(id): Observable<any> {
		this.setHeaders();
		return this.http.get(this.urlOrder + id, {
			headers: this.headers,
		});
	}
}
