import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Observable";

@Injectable({
	providedIn: "root",
})
export class AuthService {
	constructor(private httpClient: HttpClient) {}

	isLogged: boolean = false;
	url = environment.urlHost;
	urlRegistro: string = this.url + "auth/register";
	urlLogin: string = this.url + "auth/login";
	urlResetPassword: string = this.url + "auth/forgot/email";
	urlVerify: string = this.url + "auth/verify/resend/";
	urlStatus = this.url + "auth/status";
	headers: any;
	setHeader() {
		this.headers = new HttpHeaders().set(
			"Content-Type",
			"application/json"
		);
	}
	iniciarSesion(data: any): Observable<any> {
		let Json = JSON.stringify(data);

		this.setHeader();

		return this.httpClient.post(this.urlLogin, Json, {
			headers: this.headers,
		});
	}
	registroUsuario(data: any): Observable<any> {
		let Json = JSON.stringify(data);
		this.setHeader();

		return this.httpClient.post(this.urlRegistro, Json, {
			headers: this.headers,
		});
	}

	localStorage(data: any) {
		let token: string = data.accessToken;

		sessionStorage.setItem("tk_init", token);
		sessionStorage.setItem("exp_tk", data.expiresIn);
		sessionStorage.setItem("inf_tk", data.user.informacion._id);
		sessionStorage.setItem("import_data", data.user.Auth);
		sessionStorage.setItem("status", "online");
	}

	verifyToken() {
		let token: string = sessionStorage.getItem("tk_init");
		if (!token) {
			this.isLogged = false;
		} else {
			this.isLogged = true;
		}
	}

	deletelocalStorage() {
		sessionStorage.removeItem("tk_init");
		sessionStorage.removeItem("import_data");
	}

	resetPassword(data: any): Observable<any> {
		let Json = JSON.stringify(data);
		this.setHeader();
		sessionStorage.setItem("rst_psw_mail", data);
		return this.httpClient.post(this.urlResetPassword, Json, {
			headers: this.headers,
		});
	}

	verifyEmail(id): Observable<any> {
		this.setHeader();
		return this.httpClient.post(this.urlVerify + id, {
			headers: this.headers,
		});
	}
	changeStatus(status: number): Observable<any> {
		this.setHeader();
		let id = sessionStorage.getItem("import_data");
		let data = {
			id: id,
			status: status,
		};
		if (status == 0) {
			sessionStorage.setItem("status", "offline");
		}
		if (status == 1) {
			sessionStorage.setItem("status", "online");
		}
		if (status == 2) {
			window.sessionStorage.setItem("status", "away");
		}
		return this.httpClient.put(this.urlStatus, data, {
			headers: this.headers,
		});
	}
}
