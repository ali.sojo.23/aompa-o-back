import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { environment } from "../../environments/environment";
@Injectable({
	providedIn: "root",
})
export class AddressService {
	constructor(private httpClient: HttpClient) {}
	url = environment.urlHost;
	urlCountry: string = this.url + "country";

	urlCity: string = this.url + "province/country/";
	headers: any;
	setHeaders() {
		let token = sessionStorage.getItem("tk_init");
		this.headers = new HttpHeaders()
			.set("Accept", "application/json")
			.set("access-token", token);
	}
	obtenerPaises(): Observable<any> {
		this.setHeaders();
		return this.httpClient.get(this.urlCountry, { headers: this.headers });
	}

	obtenerCiudades(identificador): Observable<any> {
		this.setHeaders();

		return this.httpClient.get(this.urlCity + identificador, {
			headers: this.headers,
		});
	}
	crearCiudades(data): Observable<any> {
		this.setHeaders();

		return this.httpClient.post(this.url + "province/", data, {
			headers: this.headers,
		});
	}
	crearPais(data): Observable<any> {
		this.setHeaders();
		return this.httpClient.post(this.urlCountry, data, {
			headers: this.headers,
		});
	}
	verCiudades(identificador): Observable<any> {
		this.setHeaders();

		return this.httpClient.get(this.urlCity + "?city=" + identificador, {
			headers: this.headers,
		});
	}
}
