import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

import Pusher from 'pusher-js';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor() { }

  push(token,id){
  	let	pusher = new Pusher('24316a43edae85a18317', {
      	cluster: 'mt1',
      	forceTLS: true,
      	auth: {
    			headers: {
      			Authorization: 'Bearer' + token
    			}
    		}
    	});
  	let channel = pusher.subscribe('private-conversation.'+id);

  	channel.bind('message.posted', function (data) {
  		alert(JSON.stringify(data));
	});

  }

}


