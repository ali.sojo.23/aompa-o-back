import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { Globalization } from "@ionic-native/globalization/ngx";
import { Platform } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { TranslateService } from "@ngx-translate/core";
import { BnNgIdleService } from "bn-ng-idle";
import { AuthService } from "./servicios/auth.service";
import { AlertController } from "@ionic/angular";
import { async } from "@angular/core/testing";

@Component({
	selector: "app-root",
	templateUrl: "app.component.html",
})
export class AppComponent {
	constructor(
		private platform: Platform,
		private splashScreen: SplashScreen,
		private statusBar: StatusBar,
		public router: Router,
		private bnIdle: BnNgIdleService,
		private globalization: Globalization,
		public traductor: TranslateService,
		private auth: AuthService,
		private alert: AlertController
	) {
		this.initializeApp();
	}
	status: string;
	usuario: any;
	token() {
		let token = sessionStorage.getItem("tk_init");
		let date: number = Date.now();
		let expire: number = +sessionStorage.getItem("exp_tk");
		this.usuario = sessionStorage.getItem("import_data");
		console.log(new Date(expire));
		if (token == null) {
			this.router.navigate(["/auth/login"]);
		}
		if (date >= expire) {
			this.router.navigate(["/auth/login"]);
		}
	}

	getdata() {
		let coords: any = { lat: "", lng: "" };
		let lang: string = window.navigator.language.substr(0, 2);

		window.navigator.geolocation.getCurrentPosition(
			(resultado) => {
				coords = {
					lat: resultado.coords.latitude,
					lng: resultado.coords.longitude,
				};

				localStorage.setItem("coordinate", JSON.stringify(coords));
			},
			(error) => {
				console.log(error);
			}
		);

		localStorage.setItem("prefer_lang", lang);
	}

	setDeafultLang() {}

	async presentAlert() {
		const alert = await this.alert.create({
			backdropDismiss: false,
			header: "¡Alerta!",
			message: "No se ha detectado actividad en los ultimos minutos.",
			buttons: [
				{
					text: "online",
					role: "cancel",
					cssClass: "text-success",
					handler: (blah) => {
						this.auth
							.changeStatus(1)
							.subscribe((res) => this.setStatus());
						this.bnIdle.resetTimer();
					},
				},
				{
					text: "Mantener ausente",
					cssClass: "text-warning",
					handler: (blah) => {
						console.log("cancel");
						this.bnIdle.resetTimer();
					},
				},
				{
					text: "cerrar sesión",
					cssClass: "text-danger",
					handler: () => {
						this.auth.changeStatus(0).subscribe(
							(res) => {
								sessionStorage.clear();
								this.router.navigate(["/auth/login"]);
							},
							(err) => {
								console.log(err);
							}
						);
					},
				},
			],
		});

		await alert.present();
	}

	async ausentSession() {
		console.log(new Date(Date.now()));

		await this.bnIdle
			.startWatching(60 * 15)
			.subscribe(async (isTimedOut: boolean) => {
				if (
					sessionStorage.getItem("status") &&
					sessionStorage.getItem("import_data") &&
					sessionStorage.getItem("inf_tk")
				) {
					if (isTimedOut) {
						this.auth.changeStatus(2).subscribe(
							(res) => {
								this.setStatus();
								this.presentAlert();
								this.bnIdle.stopTimer();
							},
							(err) => {
								console.log(err);
							}
						);
					}
				}
			});
	}
	setStatus() {
		this.status = sessionStorage.getItem("status");
	}
	initializeApp() {
		this.platform.ready().then(() => {
			this.statusBar.styleDefault();
			this.splashScreen.hide();
		});
		this.token();
		this.traductor.setDefaultLang("en");
		this.getdata();
		this.ausentSession();
	}
}
