import { Component, OnInit, AfterViewInit, Input } from "@angular/core";
import { Router } from "@angular/router";
import { environment } from "../../../environments/environment";
import { from } from "rxjs";
@Component({
	selector: "app-header",
	templateUrl: "./header.component.html",
	styleUrls: ["./header.component.scss"],
})
export class HeaderComponent implements OnInit {
	constructor(private __router: Router) {
		this.comprobate();
	}
	@Input() status = "offline";
	url: string = environment.urlHost;
	usuario: any = {};
	id: number;
	show: boolean = false;
	info: string;

	comprobate() {
		this.usuario = sessionStorage.getItem("import_data");
		this.info = sessionStorage.getItem("inf_tk");
		if (this.info == "undefined") {
			this.info = null;
		}
		let status = sessionStorage.getItem("status");
		if (status) {
			this.status = status;
		}
		window.addEventListener("storage", () => {
			this.status = window.sessionStorage.getItem("status");
			console.log(this.status);
		});
	}

	cerrarSesion() {
		sessionStorage.clear();

		this.__router.navigate(["/auth/login"]);
	}

	changeInformation() {
		window.location.reload();
	}
	ngOnInit() {
		this.comprobate();
	}
}
