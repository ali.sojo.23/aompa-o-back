import { Component, OnInit } from "@angular/core";
import { AuthService } from "src/app/servicios/auth.service";
import { Router } from "@angular/router";
import { Select2OptionData } from "ng2-select2";
import { AddressService } from "src/app/servicios/address.service";
import { UsuariosService } from "src/app/servicios/usuarios.service";

@Component({
	selector: "app-create",
	templateUrl: "./create.component.html",
	styleUrls: ["./create.component.scss"],
})
export class CreateComponent implements OnInit {
	constructor(
		private userService: AuthService,
		private route: Router,
		private addressService: AddressService,
		private usuariosService: UsuariosService
	) {
		this.getCountry();
	}
	user = true;
	address = false;
	pass = Math.random().toString();
	usuario: any = {
		FirstName: "",
		LastName: "",
		email: "",
		password: this.pass,
		confirm_password: this.pass,
	};
	success: string;
	error: string;
	informacion: any = {};
	country: Array<Select2OptionData> = [];
	id: string;
	info: string;
	createUser() {
		this.userService.registroUsuario(this.usuario).subscribe(
			(res) => {
				this.success =
					"El usuario " +
					res.User.user.Nombre +
					" " +
					res.User.user.Apellido +
					" fué creado con exito." +
					"</br> será redireccionado en los proximos 5 segundos a la lista de usuarios.";

				this.id = res.User.user._id;
				this.info = res.User.user.informacion;
				this.user = false;
				this.address = true;
			},
			(err) => {
				console.log(err);
				this.error = err.error;
			}
		);
	}
	inverse(id) {
		if (id == 1) {
			this.user = true;
			this.address = false;
		}
		if (this.informacion && id == 2) {
			this.user = false;
			this.address = true;
		}
	}
	getCountry() {
		this.addressService.obtenerPaises().subscribe((res) => {
			for (let i = 0; i < res.country.length; i++) {
				if (i == 0) {
					this.country = [
						{
							id: "",
							text: "Select one country",
							disabled: true,
						},
						{
							id: res.country[i]._id,
							text: res.country[i].Country,
						},
					];
				} else {
					this.country.push({
						id: res.country[i]._id,
						text: res.country[i].Country,
					});
				}
			}
		});
	}
	saveUser() {
		let data = {
			usuario: {
				informacion: this.info,
			},
			informacion: {
				country: this.informacion.country,
			},
		};
		this.usuariosService.actualizarUsuarios(data, this.id).subscribe(
			(res) => {
				this.route.navigate([
					"/memberships/contracts",
					this.id,
					this.informacion.country,
				]);
			},
			(err) => {
				console.error(err);
			}
		);
	}
	ngOnInit() {}
}
