import { Component, OnInit } from "@angular/core";
import { UsuariosService } from "src/app/servicios/usuarios.service";
import { environment } from "src/environments/environment";
import { Router } from "@angular/router";

@Component({
	selector: "app-show",
	templateUrl: "./show.component.html",
	styleUrls: ["./show.component.scss"],
})
export class ShowComponent implements OnInit {
	constructor(private users: UsuariosService, private route: Router) {}
	email: any;
	user: any;
	host = environment.urlHost;
	search() {
		this.users.userQuery({ email: this.email }).subscribe(
			(res) => {
				console.log(res);
				this.user = res;
			},
			(err) => {
				console.log(err);
			}
		);
	}
	avatar(avatar) {
		return this.host + avatar;
	}
	next(id, country) {
		this.route.navigate(["/memberships/contracts", id, country]);
	}
	ngOnInit() {}
}
