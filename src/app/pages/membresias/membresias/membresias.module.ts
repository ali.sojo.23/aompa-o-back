import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { MembresiasPage } from "./membresias.page";
import { ModalComponent } from "./modal/modal.component";

const routes: Routes = [
	{
		path: "",
		component: MembresiasPage,
	},
];

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		RouterModule.forChild(routes),
	],
	declarations: [MembresiasPage, ModalComponent],
	entryComponents: [ModalComponent],
})
export class MembresiasPageModule {}
