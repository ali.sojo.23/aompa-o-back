import { Component, OnInit, Input } from "@angular/core";
import { ModalController } from "@ionic/angular";
import { OrdersService } from "src/app/servicios/accounting/orders.service";
import { Router } from "@angular/router";

@Component({
	selector: "app-modal",
	templateUrl: "./modal.component.html",
	styleUrls: ["./modal.component.scss"],
})
export class ModalComponent implements OnInit {
	@Input() membresia: any;
	@Input() usuario: string;
	constructor(
		private modal: ModalController,
		private orders: OrdersService,
		private route: Router
	) {}
	dismiss() {
		// using the injected ModalController this page
		// can "dismiss" itself and optionally pass back data
		this.modal.dismiss({
			dismissed: true,
		});
	}
	crearPedido() {
		let order = {
			membresia: this.membresia,
			usuario: this.usuario,
		};

		this.orders.create(order).subscribe(
			(res) => {
				this.dismiss();
				setTimeout(() => {
					this.route.navigate(["/accounting/orders", res._id]);
				}, 2000);
			},
			(err) => {
				console.log(err);
			}
		);
	}
	ngOnInit() {}
}
