import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { MembershipsService } from "src/app/servicios/settings/memberships.service";
import { ModalController } from "@ionic/angular";
import { ModalComponent } from "./modal/modal.component";
ModalComponent;
@Component({
	selector: "app-membresias",
	templateUrl: "./membresias.page.html",
	styleUrls: ["./membresias.page.scss"],
})
export class MembresiasPage implements OnInit {
	constructor(
		private route: ActivatedRoute,
		private memberships: MembershipsService,
		private modal: ModalController
	) {
		this.get();
	}
	user = this.route.snapshot.paramMap.get("user");
	pais = this.route.snapshot.paramMap.get("country");
	members: any;
	error: string;
	get() {
		this.memberships.obtain().subscribe(
			(res) => {
				console.log(res);

				this.members = res;
			},
			(err) => {
				console.log(err);
			}
		);
	}
	servicio(i) {
		if (i == 1) {
			return "Servicios en Domicilios";
		}
		if (i == 2) {
			return "Servicios en Hospital";
		}
		if (i == 3) {
			return "Servicios en Hospital privado";
		}
	}
	async showModal(id) {
		let modal = await this.modal.create({
			component: ModalComponent,
			componentProps: {
				membresia: id,
				usuario: this.user,
			},
		});

		return modal.present();
	}

	ngOnInit() {}
}
