import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { DropzoneModule } from "ngx-dropzone-wrapper";
import { DROPZONE_CONFIG } from "ngx-dropzone-wrapper";
import { DropzoneConfigInterface } from "ngx-dropzone-wrapper";
import { AssetsModule } from "src/app/modules/assets/assets.module";
import { LoadPage } from "./load.page";
import { environment } from "src/environments/environment";

const routes: Routes = [
	{
		path: "",
		component: LoadPage,
	},
];

const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
	// Change this to your upload POST address:
	url: environment.urlHost2 + "marketing/leads",
	maxFilesize: 1,
	acceptedFiles: ".csv",
	headers: { "access-token": sessionStorage.getItem("tk_init") },
};

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		RouterModule.forChild(routes),
		DropzoneModule,
		AssetsModule,
	],
	declarations: [LoadPage],
	providers: [
		{
			provide: DROPZONE_CONFIG,
			useValue: DEFAULT_DROPZONE_CONFIG,
		},
	],
})
export class LoadPageModule {}
