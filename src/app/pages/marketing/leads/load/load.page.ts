import { Component, OnInit } from "@angular/core";

@Component({
	selector: "app-load",
	templateUrl: "./load.page.html",
	styleUrls: ["./load.page.scss"],
})
export class LoadPage implements OnInit {
	constructor() {}
	error;
	success;
	ngOnInit() {}
	onUploadError(event) {
		this.error = event[1];
	}
	onUploadSuccess(event) {
		this.success = event[1].message;
	}
	drop(event) {
		console.log("droping", event.target);
	}
}
