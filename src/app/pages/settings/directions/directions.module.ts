import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { DirectionsPage } from "./directions.page";
import { ModalComponent } from "./modal/modal.component";
import { AssetsModule } from "src/app/modules/assets/assets.module";

const routes: Routes = [
	{
		path: "",
		component: DirectionsPage,
	},
];

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		RouterModule.forChild(routes),
		AssetsModule,
	],
	declarations: [DirectionsPage, ModalComponent],
	entryComponents: [ModalComponent],
})
export class DirectionsPageModule {}
