import {
	Component,
	OnInit,
	Input,
	OnChanges,
	SimpleChanges,
} from "@angular/core";
import { RatesService } from "src/app/servicios/settings/rates.service";

@Component({
	selector: "app-crud",
	templateUrl: "./crud.component.html",
	styleUrls: ["./crud.component.scss"],
})
export class CrudComponent implements OnInit, OnChanges {
	constructor(private rates: RatesService) {}
	@Input() pais: any;

	temporal: number = 1;
	prev: any = {
		rate: "",
		code: "",
	};
	rate: any = {
		rate: "",
		code: "",
		tax: "",
		country: this.pais,
	};
	edit: boolean = false;
	error: string;
	success: string;
	getRates(id) {
		this.rates.getRate(id).subscribe(
			(res) => {
				if (res) {
					this.rate = res;
					this.prev = res;
					this.edit = true;
					this.error = "";
					this.success = "";
					console.log(this.edit, this.rate, this.prev);
				} else {
					this.rate = {
						rate: "",
						code: "",
						country: this.pais._id,
					};

					this.prev = this.rate;
					this.edit = false;
					this.error =
						"No existe un rate creado para el " +
						this.pais.Country +
						" por favor crear uno nuevo.";
					this.success = "";
					console.log(this.edit, this.rate, this.prev);
				}
			},
			(err) => {
				console.error(err);
			}
		);
	}
	createRate() {
		let rate = this.rate;

		this.rates.postRate(rate).subscribe(
			(res) => {
				this.success = "El rate se ha guardado satisfactioriamente";
				this.getRates(this.pais._id);
			},
			(err) => {
				console.error(err);
			}
		);
	}
	updateRate(id) {
		this.rates.putRate(id, this.rate).subscribe(
			(res) => {
				this.success = "El rate se ha actualizado satisfactioriamente";
			},
			(err) => {
				console.log(err);
			}
		);
	}
	ngOnChanges(changes: SimpleChanges) {
		let data = changes.pais.currentValue;
		if (data) {
			this.getRates(data._id);
		}
		console.log(changes.rate);
	}

	ngOnInit() {}
}
