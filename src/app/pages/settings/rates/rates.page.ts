import { Component, OnInit, SimpleChanges, OnChanges } from "@angular/core";
import { AddressService } from "src/app/servicios/address.service";
import { RatesService } from "src/app/servicios/settings/rates.service";

@Component({
	selector: "app-rates",
	templateUrl: "./rates.page.html",
	styleUrls: ["./rates.page.scss"],
})
export class RatesPage implements OnInit, OnChanges {
	constructor(
		private directions: AddressService,
		private rates: RatesService
	) {
		this.getCountry();
	}
	rate: any;
	pais: any;
	countries: any;
	getCountry() {
		this.directions.obtenerPaises().subscribe(
			(res) => {
				this.countries = res.country;
				console.log(this.countries);
			},
			(err) => {
				console.log(err);
			}
		);
	}

	ngOnChanges(changes: SimpleChanges) {
		console.log(changes.pais.currentValue);
	}
	ngOnInit() {}
}
