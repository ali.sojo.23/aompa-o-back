import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { RatesPage } from "./rates.page";
import { AssetsModule } from "src/app/modules/assets/assets.module";
import { CrudComponent } from "./crud/crud.component";

const routes: Routes = [
	{
		path: "",
		component: RatesPage,
	},
];

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		RouterModule.forChild(routes),
		AssetsModule,
	],
	declarations: [RatesPage, CrudComponent],
})
export class RatesPageModule {}
