import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { EditPage } from "./edit.page";
import { AssetsModule } from "src/app/modules/assets/assets.module";

const routes: Routes = [
	{
		path: "",
		component: EditPage,
	},
];

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		RouterModule.forChild(routes),
		AssetsModule,
	],
	declarations: [EditPage],
})
export class EditPageModule {}
