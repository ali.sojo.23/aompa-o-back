import { Component, OnInit } from "@angular/core";
import { MembershipsService } from "src/app/servicios/settings/memberships.service";

@Component({
	selector: "app-show",
	templateUrl: "./show.page.html",
	styleUrls: ["./show.page.scss"],
})
export class ShowPage implements OnInit {
	constructor(private memberships: MembershipsService) {
		this.get();
	}
	members: any;
	error: string;
	get() {
		this.memberships.obtain().subscribe(
			(res) => {
				this.members = res;
			},
			(err) => {
				console.log(err);
			}
		);
	}
	delete(id) {
		this.memberships.DeleteOne(id).subscribe(
			(res) => {
				if (res)
					this.error = "La membresía fue eliminada correctamente";
				this.get();
			},
			(err) => {
				console.log(err);
			}
		);
	}
	servicio(i) {
		if (i == 1) {
			return "Servicios en Domicilios";
		}
		if (i == 2) {
			return "Servicios en Hospital";
		}
		if (i == 3) {
			return "Servicios en Hospital privado";
		}
	}

	ngOnInit() {}
}
