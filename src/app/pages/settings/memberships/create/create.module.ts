import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { CreatePage } from "./create.page";
import { AssetsModule } from "src/app/modules/assets/assets.module";
import { Select2Module } from "ng2-select2";

const routes: Routes = [
	{
		path: "",
		component: CreatePage,
	},
];

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		RouterModule.forChild(routes),
		AssetsModule,
		Select2Module,
	],
	declarations: [CreatePage],
})
export class CreatePageModule {}
