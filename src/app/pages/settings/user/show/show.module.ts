import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { ShowPage } from "./show.page";
import { DeleteComponent } from "./delete/delete.component";

const routes: Routes = [
	{
		path: "",
		component: ShowPage,
	},
];

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		RouterModule.forChild(routes),
	],
	declarations: [ShowPage, DeleteComponent],
})
export class ShowPageModule {}
