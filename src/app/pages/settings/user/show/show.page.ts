import { Component, OnInit } from "@angular/core";
import { UsuariosService } from "src/app/servicios/usuarios.service";
import { environment } from "src/environments/environment";

@Component({
	selector: "app-show",
	templateUrl: "./show.page.html",
	styleUrls: ["./show.page.scss"],
})
export class ShowPage implements OnInit {
	constructor(private usuario: UsuariosService) {
		this.get();
	}
	users: any;
	host = environment.urlHost;
	show = 0;
	get() {
		this.usuario.obtenerUsuarios().subscribe(
			(res) => {
				this.users = res.user;
			},
			(err) => {
				console.log(err);
			}
		);
	}
	filter(role) {
		if (role == this.show) {
			return true;
		} else {
			return false;
		}
	}
	roleClass(role) {
		switch (role) {
			case 1:
				return "status-green";
				break;
			case 10:
				return "status-blue";
				break;
			case 100:
				return "status-red";
				break;
			case 1000:
				return "status-orange";
				break;
			default:
				break;
		}
	}
	role(role) {
		switch (role) {
			case 1:
				return "Administrador";
				break;
			case 10:
				return "Empleado";
				break;
			case 100:
				return "Acompañante";
				break;
			case 1000:
				return "Usuario";
				break;

			default:
				break;
		}
	}

	ngOnInit() {}
}
