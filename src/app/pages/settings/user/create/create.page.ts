import { Component, OnInit } from "@angular/core";
import { AuthService } from "src/app/servicios/auth.service";
import { Router } from "@angular/router";

@Component({
	selector: "app-create",
	templateUrl: "./create.page.html",
	styleUrls: ["./create.page.scss"],
})
export class CreatePage implements OnInit {
	constructor(private userService: AuthService, private route: Router) {}
	pass = Math.random().toString();
	user: any = {
		FirstName: "",
		LastName: "",
		email: "",
		password: this.pass,
		confirm_password: this.pass,
		Role: "",
	};
	error: any;
	success: any;
	createUser() {
		this.userService.registroUsuario(this.user).subscribe(
			(res) => {
				this.success =
					"El usuario " +
					res.User.user.Nombre +
					" " +
					res.User.user.Apellido +
					" fué creado con exito." +
					"</br> será redireccionado en los proximos 5 segundos a la lista de usuarios.";
				setTimeout(() => {
					this.route.navigate(["/settings/users"]);
				}, 5000);
			},
			(err) => {
				console.log(err);

				this.error = err.error;
			}
		);
	}
	ngOnInit() {}
}
