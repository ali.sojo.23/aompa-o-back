import {
	Component,
	OnInit,
	AfterContentChecked,
	OnDestroy,
} from "@angular/core";
import { UsuariosService } from "../../../servicios/usuarios.service";
import { ActivatedRoute } from "@angular/router";
import { EstudiosService } from "../../../servicios/estudios.service";
import { ExperienceService } from "../../../servicios/experience.service";
import { AuthService } from "../../../servicios/auth.service";
import { TranslateService } from "@ngx-translate/core";
import { environment } from "../../../../environments/environment";
import { LoadingController, AlertController } from "@ionic/angular";

@Component({
	selector: "app-perfil",
	templateUrl: "./perfil.page.html",
	styleUrls: ["./perfil.page.scss"],
})
export class PerfilPage implements OnInit, AfterContentChecked, OnDestroy {
	constructor(
		private __usuario: UsuariosService,
		private __route: ActivatedRoute,
		private __estudios: EstudiosService,
		private __exp: ExperienceService,
		private __auth: AuthService,
		private loader: LoadingController,
		private __alert: AlertController,
		private traductor: TranslateService
	) {
		this.obtenerUsuario();
		this.setLang();
	}
	url = environment.urlHost;
	usuario: any = {
		FirstName: "",
		LastName: "",
		email: "",
	};
	experiencia: any;
	estudios: any;
	identificador: string;

	ngOnInit() {
		this.obtenerUsuario();
		this.urlSlug();
	}
	ngAfterContentChecked() {}

	urlSlug() {
		this.identificador = this.__route.snapshot.paramMap.get("id");
	}

	doRefresh(event) {
		this.obtenerUsuario();
		setTimeout(() => {
			event.target.complete();
		}, 2000);
	}

	async obtenerUsuario() {
		let load = await this.loader.create({ message: "por favor espere" });
		let Alert = await this.__alert.create({
			header: "Alerta!!",
			message:
				"Error al conectarse al descargar la información del servidor",
			buttons: [
				{
					text: "Reload",
					role: "cancel",
					cssClass: "secondary",
					handler: () => {
						this.obtenerUsuario();
					},
				},
			],
		});

		load.present();
		Alert.dismiss();
		this.urlSlug();

		this.__usuario.mostrarUsuario(this.identificador).subscribe(
			(resultado) => {
				load.dismiss();
				this.usuario = resultado.user;
			},
			(error) => {
				load.dismiss();
				Alert.present();
				console.error(error);
			}
		);
	}

	obtenesEstudios(identificador) {
		this.__estudios.getEstudios(identificador).subscribe(
			(resultado) => {
				this.estudios = resultado.studies;
			},
			(error) => {
				console.log(error);
			}
		);
	}
	getExperiencia(identificador) {
		this.__exp.get(identificador).subscribe(
			(resultado) => {
				this.experiencia = resultado.experiencia;
			},
			(error) => {
				console.log(error);
			}
		);
	}
	async VerificarCorreo(identificador) {
		let Alert = await this.__alert.create({
			header: "",
			message: "Enviando correo de verificación de usuario",
			buttons: [
				{
					text: "Ok",
					role: "aubmit",
					cssClass: "secondary",
				},
			],
		});
		Alert.present();

		this.__auth.verifyEmail(identificador).subscribe(
			(resultado) => {
				console.log(resultado);
				this.usuario.email_verified_at = "verified";
			},
			(error) => {
				console.log(error);
			}
		);
	}

	setLang() {
		this.traductor.setDefaultLang("en");
		let lang = localStorage.getItem("prefer_lang");
		this.traductor.use(lang);
	}

	ngOnDestroy() {}
}
